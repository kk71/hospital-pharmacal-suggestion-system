#coding=utf-8
__author__ = 'kk'
'''

classes to handle http requests from tornado server.

'''

import traceback
from logging import info, debug, warn, error
from tornado.web import RequestHandler
from utils import MessageBox
from lxml import etree


class mk_query(RequestHandler):
    """
    query mk DLL through http request.
    """
    def get(self):
        # get data from query string
        text = self.get_query_argument("text", "无消息")

        # run the function from the dll
        MessageBox(text=text)

        # example to give the response xml
        base = etree.Element("base_tag")
        haha = etree.SubElement(base, "sub_tag")
        haha.text = "haha value"

        # make response and end the request
        self.finish(etree.tostring(base))
