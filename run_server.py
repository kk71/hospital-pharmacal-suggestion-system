#!/usr/bin/env python
#coding=utf-8
__author__ = 'kk'

'''

a dll broker sample using tornado
written by kk(fkfkbill@gmail.com, https://github.com/kk71)

how to run?

python runserver.py

for details, see README.md

'''

from logging import info, warn
from tornado.ioloop import IOLoop
from tornado.httpserver import HTTPServer
from tornado.web import Application
from tornado.log import enable_pretty_logging


# debug/prod switch
DEBUG = True


def make_app():
    import urls
    return Application(urls.urls, autoreload=False, debug=DEBUG)


def main(process=4, listen_port=80):
    """
    start the server.
    :return:
    """
    info("starting the sample DLL broker...")
    enable_pretty_logging()
    app = make_app()
    server = HTTPServer(app)
    server.bind(listen_port)
    if DEBUG:
        server.start()
        warn("WARNING: DEBUG is {debug}".format(debug=DEBUG))
    else:
        server.start(process)
    IOLoop.current().start()


if __name__=="__main__":
    main()
