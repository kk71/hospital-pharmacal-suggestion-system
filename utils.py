#coding=utf-8
__author__ = 'kk'
'''

a file to include some utilities, such as DLL operations

'''


import traceback
from logging import info, debug, warn, error
from ctypes import c_int, WINFUNCTYPE, windll, WinDLL
from ctypes.wintypes import HWND, LPCSTR, UINT


# firstly, create an object linked to your DLL

user32 = WinDLL("user32.dll")

# the prototype to your function(in C as example)
proto = WINFUNCTYPE(c_int, HWND, LPCSTR, LPCSTR, UINT)

# now we are going to create a function-wrapper for your function in python
# it makes the function is used as in native python

# the parameters and default value
paramflags = (1, "hwnd", 0), (1, "text", "Hi"), (1, "caption", ""), (1, "flags", 0)

# okay
MessageBox = proto(("MessageBoxA", windll.user32), paramflags)

# usage:
#
# MessageBox(text="Hello world!~", caption="heihei")
