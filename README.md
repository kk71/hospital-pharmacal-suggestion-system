医院药学建议系统
=============

## 初步功能

已有一个DLL中存放了一个药物互相作用的查询系统,通过python调用查询,并获得返回建议.

通过一个tornado框架将服务做成接口,在windows server上跑.

> windows server + python2.7 + tornado


## 如何部署

修改run_server.py文件中的 DEBUG=True

然后在windows下:

    python runserver.py

